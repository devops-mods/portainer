# Portainer

## How to install
Set the permission to execute run.sh
```console
chmod u+x run.sh
```
Execute run.sh
```console
./run.sh
```
### How to use run.sh

```console
 Use ./run.sh to start stack 
  -c or --clean to clean config files and docker volumes and networks 
  -r or --reset to purge config files 
  -h or --help to display help
```

## What do run.sh

1. Check if ./config/env exist
2. If previous steps is not configure, asks the questions to set domain name
3. Load env variable
4. Run docker-compose up -d
