#!/bin/bash

FILE=./config/env
function init() {
  if [ ! -f "$FILE" ]; then
    mkdir ./config
    while true; do
      read -p "Set your domain name: " domain
      echo
      if [[ "$domain" =~ ^[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$ ]]; then
        echo "domain=$domain" >$FILE
        break
      else
        echo "Domain address $domain is invalid."
      fi
    done
  fi
}

function start() {
  export $(grep -v '^#' $FILE | xargs)
  docker-compose up -d
  if [ $? -eq 0 ]; then
    echo -e "\e[92m Stack successfully start. \n
      Url: https://portainer.$domain \e[39m"
  fi
}

function reset() {
  rm $FILE
  rm ./config/*/env
}

function clean() {
  reset
  docker-compose down -v
}

function main() {
  init
  start
}
HELP="
\e[92m Use ./run.sh to start stack \n
\e[31m -c or --clean to clean config files and docker volumes and networks \n
\e[34m -r or --reset to purge config files \n
\e[39m -h or --help to display help"

if [[ "$#" -eq 0 ]]; then
  main
fi

while [[ "$#" -gt 0 ]]; do
  case $1 in
  -c | --clean)
    clean
    shift
    ;;
  -r | --reset)
    reset
    shift
    ;;
  -h | --help)
    echo -e $HELP
    exit
    ;;
  *)
    echo "Unknown parameter passed: $1"
    exit 1
    ;;
  esac
  shift
done
